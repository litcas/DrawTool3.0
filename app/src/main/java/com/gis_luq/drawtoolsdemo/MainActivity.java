package com.gis_luq.drawtoolsdemo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import android.widget.Button;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapOnTouchListener;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.core.map.Graphic;
import com.esri.core.table.TableException;
import com.gis_luq.lib.Draw.DrawEvent;
import com.gis_luq.lib.Draw.DrawEventListener;
import com.gis_luq.lib.Draw.DrawTool;

import java.io.FileNotFoundException;

public class MainActivity extends Activity implements DrawEventListener {

    private Context context;
    private MapView mapView = null;
    private ArcGISTiledMapServiceLayer arcGISTiledMapServiceLayer = null;
    private GraphicsLayer graphicsLayer = null;

    private Graphic selectGraphic = null;
    private DrawTool drawTool;

    public MapOnTouchListener mapDefaultOnTouchListener;//默认点击事件
    public DrawEventListener drawEventListener;//要素绘制点击事件


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        this.mapView = (MapView)this.findViewById(R.id.map);//设置UI和代码绑定

        String strMapUrl="http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineCommunity/MapServer";
        this.arcGISTiledMapServiceLayer = new ArcGISTiledMapServiceLayer(strMapUrl);
        this.mapView.addLayer(arcGISTiledMapServiceLayer);

        graphicsLayer = new GraphicsLayer();
        this.mapView.addLayer(graphicsLayer);

        // 初始化DrawTool实例
        this.drawTool = new DrawTool(this.mapView);
        // 将本Activity设置为DrawTool实例的Listener
        this.drawTool.addEventListener(this);

        //设置地图事件
        mapDefaultOnTouchListener = new MapOnTouchListener(this.mapView.getContext(), this.mapView);
        drawEventListener = this;

        ToolsOnClickListener toolsOnClickListener = new ToolsOnClickListener(context,drawTool,selectGraphic,mapView);
        Button btnDrawPoint = (Button)this.findViewById(R.id.btnDrawPoint);
        btnDrawPoint.setOnClickListener(toolsOnClickListener);

        Button btnDrawPolyline = (Button)this.findViewById(R.id.btnDrawPolyline);
        btnDrawPolyline.setOnClickListener(toolsOnClickListener);

        Button btnDrawFreePolyline = (Button)this.findViewById(R.id.btnDrawFreePolyline);
        btnDrawFreePolyline.setOnClickListener(toolsOnClickListener);

        Button btnDrawPolygon = (Button)this.findViewById(R.id.btnDrawPolygon);
        btnDrawPolygon.setOnClickListener(toolsOnClickListener);

        Button btnDrawFreePolygon = (Button)this.findViewById(R.id.btnDrawFreePolygon);
        btnDrawFreePolygon.setOnClickListener(toolsOnClickListener);

        Button btnDrawCircle = (Button)this.findViewById(R.id.btnDrawCircle);
        btnDrawCircle.setOnClickListener(toolsOnClickListener);

        Button btnDrawEnvlope = (Button)this.findViewById(R.id.btnDrawEnvlope);
        btnDrawEnvlope.setOnClickListener(toolsOnClickListener);

        Button btnDrawEditor = (Button)this.findViewById(R.id.btnDrawSave);
        btnDrawEditor.setOnClickListener(toolsOnClickListener);

        Button btnDrawUndo = (Button)this.findViewById(R.id.btnDrawUndo);
        btnDrawUndo.setOnClickListener(toolsOnClickListener);

        Button btnDrawDeleteNode = (Button)this.findViewById(R.id.btnDrawDeleteNode);
        btnDrawDeleteNode.setOnClickListener(toolsOnClickListener);

    }

    @Override
    public void handleDrawEvent(DrawEvent event) throws TableException, FileNotFoundException {
        // 将画好的图形（已经实例化了Graphic），添加到drawLayer中并刷新显示
        this.graphicsLayer.addGraphic(event.getDrawGraphic());
        // 修改点击事件为默认
        this.mapView.setOnTouchListener(mapDefaultOnTouchListener);
    }
}
